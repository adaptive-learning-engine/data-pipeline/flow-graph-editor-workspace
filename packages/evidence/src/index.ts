import Graph from 'graphology';
import type { DirectedGraph } from 'graphology';
import { of } from 'rxjs';

// import {
//   EvidencePipelineNetwork,
//   IEdgeInfo,
//   IEvidencePipelineData,
//   IOperatorConfig,
//   IOperatorInstantiation,
// } from '@adlete/evidence-pipeline';
// import { input } from '@adlete/evidence-pipeline/operators/input';
// import * as rxConnectors from '@adlete/evidence-pipeline/rx/operators';

// // TODO: use proper import once graphology properly uses exports
// // eslint-disable-next-line @typescript-eslint/no-explicit-any
// const DirectedGraphX = (Graph as any).DirectedGraph as typeof DirectedGraph;

// const graph = new DirectedGraphX<IOperatorConfig, IEdgeInfo>();
// graph.addNode('of', { operator: 'of', args: [1, 2, 3, 4] });
// graph.addNode('map', { operator: 'map', args: ['foobar'] });
// graph.addNode('input', { operator: 'input', args: ['inputA'] });
// graph.addDirectedEdgeWithKey('of-map', 'of', 'map');

// console.log(graph);

// const pipelineData: IEvidencePipelineData = { graph };

// const instantiationData: IOperatorInstantiation = {
//   inputs: { inputA: of([1, 2, 3]) },
//   functions: { map: { foobar: (x: unknown) => x } },
// };

// const connectors = { ...rxConnectors, input };

// const network = new EvidencePipelineNetwork(instantiationData, connectors);
// network.addPipeline(pipelineData);
