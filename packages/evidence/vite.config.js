import path from 'path';

import dotenv from 'dotenv';

import { defineConfig } from 'vite';
// import reactPlugin from '@vitejs/plugin-react';
// import bundleVisualizer from 'rollup-plugin-visualizer';
// import analyze from 'rollup-plugin-analyzer';

const EMPTY_FILE = 'export default {}';
const EMPTY_FILE_NAME = '\0rollup_plugin_ignore_empty_module_placeholder';

function ignoreModules(list) {
  return {
    enforce: 'pre',
    resolveId(importee) {
      return importee === EMPTY_FILE_NAME || list.includes(importee) ? EMPTY_FILE_NAME : null;
    },
    load(id) {
      return id === EMPTY_FILE_NAME ? EMPTY_FILE : null;
    },
  };
}

// load .env-File into environment variables
dotenv.config();

const { VITE_VLE_PORT } = process.env;

const port = VITE_VLE_PORT || 3000;

const aliasPackages = [
  // '@fki/editor-core',
  // '@fki/plugin-based-app',
  // '@fki/plugin-system',
  // '@adlete-vle/lr-core',
  // '@adlete-vle/lr-core-components',
  // '@kiwi-al/math-core',
  // '@kiwi-al/math-core-components',
  // '@kiwi-al/lr-naive-bayes',
  // '@adlete-vle/utils',
  // '@adlete-vle/vle-core',
];

function addAliasPackages(aliases, aliasPackages) {
  aliasPackages.forEach((pkg) => {
    aliases[pkg] = path.join(path.dirname(require.resolve(`${pkg}/package.json`)), 'src');
  });
}

const alias = {};
addAliasPackages(alias, aliasPackages);

// https://vitejs.dev/config/
export default defineConfig({
  // base: './',

  // root: path.join(__dirname, 'public'),
  // build: {
  //   commonjsOptions: {
  //     transformMixedEsModules: true,
  //   },
  //   // these are the minimum versions supporting top-level-await
  //   target: ['es2021'], // es2021
  //   rollupOptions: {
  //     input: {
  //       main: path.join(__dirname, 'index.html'),
  //     },
  //   },
  // },
  plugins: [
    // ignoreModules(['plotly.js-dist-min']),
    // bundleVisualizer({
    //   emitFile: true,
    //   file: './dist/stats.html',
    // }),
    // analyze({ summaryOnly: true, limit: 50 }),
  ],
  resolve: {
    // resolve these modules directly from typescript source code (not builds!)
    alias: alias,
  },
  // server: {
  //   port: port,
  // },
});
